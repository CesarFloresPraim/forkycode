RESET="$(tput sgr0)"
GREEN="$(tput setaf 2)"

echo  "${GREEN}Getting last changes...${RESET}" 
git pull

echo  "${GREEN}Making migrations...${RESET}"
docker-compose exec web bash -c "python3 manage.py makemigrations; \
    python manage.py migrate;"

echo  "${GREEN}Restarting containers...${RESET}"
docker-compose restart

echo "All done :)"