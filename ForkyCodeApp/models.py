from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
class Forker(models.Model):
    email = models.CharField(max_length=100, db_column='user', primary_key=True)
    password = models.CharField(max_length=100)
    firstname = models.CharField(max_length=30)
    lastname = models.CharField(max_length=30)
    points = models.IntegerField(default=0)
    favorite_language = models.CharField(max_length=15, default='No language')

    def __str__(self):
        return self.email


class DataType(models.Model):
    TYPES = (
        ('null', 'null'),
        ('void', 'void'),
        ('integer', 'integer'),
        ('string', 'string'),
        ('float', 'float'),
        ('char', 'char'),
        ('array', 'array'),
        ('dict', 'dict'),
    )
    type = models.CharField(max_length=20, choices=TYPES, default='null')

    def __str__(self):
        return self.type


class Category(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Challenge(models.Model):
    LEVELS = (
        ('Hard', 'Hard'),
        ('Medium', 'Medium'),
        ('Easy', 'Easy'),
        ('None', 'None')
    )
    description = models.CharField(max_length=10000)
    level = models.CharField(max_length=10, choices=LEVELS, default='None')
    function_name = models.CharField(max_length=20)
    points = models.IntegerField(validators=[MaxValueValidator(500), MinValueValidator(1)], default=1)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)

    def __str__(self):
        return self.function_name + "_id=" + str(self.id)


class TestCase(models.Model):
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE)
    is_real = models.BooleanField(default=False)
    correct_result = models.CharField(max_length=10000)
    correct_type = models.ForeignKey(DataType, on_delete=models.PROTECT)

    def __str__(self):
        return self.challenge.function_name + "_case_id=" + str(self.id)


class ParameterConfig(models.Model):
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE)
    parameter_name = models.CharField(max_length=20, null=False)
    parameter_type = models.ForeignKey(DataType, on_delete=models.PROTECT)
    order = models.IntegerField(default=-1)

    def __str__(self):
        return self.challenge.function_name + "_id=" + str(self.challenge.id) + " param: " + self.parameter_name


class Parameter(models.Model):
    test_case = models.ForeignKey(TestCase, on_delete=models.CASCADE)
    parameter_config = models.ForeignKey(ParameterConfig, on_delete=models.CASCADE)
    parameter_value = models.CharField(max_length=100)

    def __str__(self):
        return self.parameter_config.challenge.function_name + "_test_id=" + str(self.test_case.id) + " param: " + self.parameter_config.parameter_name + " val: " + self.parameter_value


class ForumEntry(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    author = models.ForeignKey(Forker, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True,null=False)
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)

    def __str__(self):
        return self.title


class ForumComment(models.Model):
    description = models.CharField(max_length=1000)
    entry = models.ForeignKey(ForumEntry, on_delete=models.CASCADE)
    author = models.ForeignKey(Forker, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True, null=False)
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)
    def __str__(self):
        return self.entry.title + '_' + str(self.id)


class CompletedChallenges(models.Model):
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE)
    forker = models.ForeignKey(Forker, on_delete=models.CASCADE)

    def __str__(self):
        return self.challenge.function_name + "_" + self.forker.email
