from django.apps import AppConfig


class ForkycodeappConfig(AppConfig):
    name = 'ForkyCodeApp'
