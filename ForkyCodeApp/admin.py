from django.contrib import admin
from ForkyCodeApp.models import *

# Register your models here.
admin.site.register(Forker)
admin.site.register(DataType)
admin.site.register(Challenge)
admin.site.register(ParameterConfig)
admin.site.register(Parameter)
admin.site.register(TestCase)
admin.site.register(ForumEntry)
admin.site.register(ForumComment)
admin.site.register(Category)
admin.site.register(CompletedChallenges)
