$(document).ready(function () {
    $(".main-space").height(100 + '%');

    //Only show main div
    let mainDivId = 'forky-main-content-space';
    $(".main-space").not("#" + mainDivId).hide();

});

//Change main content active div
function changeActiveDiv(activeDivId, clicked = null) {
    //Hide all divs
    $(".main-space").hide();

    if (activeDivId === 'leaderbords-content') {
        $("#get_leader_data").submit();
    }

    if (activeDivId === 'forum-content' || activeDivId === 'forum-content-mobile') {
        activeDivId = 'forum-content';
        let page = $(clicked).data("value");
        getForumDataPage(page, activeDivId);

    } else if (activeDivId === 'forum-entry-content') {
        let id = $(clicked).data("value");
        getForumEntryData(id, activeDivId);

    }else if (activeDivId === 'challenges-list-content') {
        loadChallenges();
    }else if (activeDivId === 'account-content') {
        loadProfile();
    }else{
        showDiv(activeDivId)
    }
}

function showDiv(activeId) {
    let content = $("#" + activeId + '-space');
    let color = content.css( "background-color" );
    $("#main-div-body").css("background-color", color);
    //Show active div
    content.show();
    $(window).scrollTop(0,0);
}

function loadProfile() {
    $("#get_profile_data").submit();
}

function getForumEntryData(id, activeId) {
    const form = $("#get_forum_entry_data");
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: {id : id},
        dataType: 'json',
        success: function (respuesta) {
            $("#entry_info").html(respuesta['html_entry']);
            $("#comments_list").html(respuesta['html_comments']);
            showDiv(activeId)
        },
        error: function (error_respuesta) {
            //console.log(error_respuesta.responseText)
        }
    })
}

function getForumDataPage(page, activeId) {
    const form = $("#get_forum_data");
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: {page : page},
        dataType: 'json',
        success: function (respuesta) {
            $("#list_forum").html(respuesta['html_forum']);
            $("#pagination").html(respuesta['html_pagination']);
            showDiv(activeId)
        },
        error: function (error_respuesta) {
            //console.log(error_respuesta)
        }
    })
}

function submitLogout() {
    $("#logout").submit();
}

function loadChallenges() {
    $("#load_challenges").submit();
}

$(document).ready(function () {
    $('.dropdown-trigger').dropdown({
        coverTrigger: false
    });
    $('.sidenav').sidenav();
    $('.collapsible').collapsible();

    $("#load_challenges").submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            dataType: 'json',
            success: function (respuesta) {
                $("#challenges_collection").html(respuesta['html_code'])
                showDiv('challenges-list-content')
            },
            error: function (error_respuesta) {
            }
        });
    });

    /*----Busca a los empleados----*/
    $("#get_leader_data").submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function (respuesta) {
                $("#list_leaderbords").html(respuesta['html_codigo'])
            },
            error: function (error_respuesta) {
            }
        })
    });

    $("#create-entry-form").submit(function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function (respuesta) {
                changeActiveDiv('forum-content')
            },
            error: function (error_respuesta) {
                //alert("Something when wrong please try again later.")
                changeActiveDiv('forum-content')
            },

        }).done(() => {
            $("#desc-input").val('');
            $("#title-input").val('');
        })
        //SubForm();
    });
    $("#comment_form").submit(function (e) {
        e.preventDefault();
        let entry_id = $('#entry-info').data("value");
        let info = $(this).serialize();
        info += "&entry_id=" + entry_id;
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: info,
            dataType: 'json',
            success: function (respuesta) {
                getForumEntryData(entry_id, 'forum-entry-content')
            },
            error: function (error_respuesta) {
                //console.log(error_respuesta.responseText);
                //alert("Something when wrong please try again later.");
            }
        }).done(() => {
           $("#reply").val('');
        })
    });

    $("#get_profile_data").submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function (respuesta) {
                $("#profile_info").html(respuesta['html_info']);
                $("#profile_extra").html(respuesta['html_extra']);
                showDiv('account-content')
            },
            error: function (error_respuesta) {
            }
        })
    });
});

