function checkIfMobileUser() {
    if (screen.width < 667) {
        document.getElementById('overlay').style.display = "block"
    }
}

function runTests() {
    makePost(false)
}

function sendResults() {
    makePost(true);
}

function openTab(evt, tabName) {
  // Declare all variables
  let i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}


function accordion() {
    let acc = document.getElementsByClassName("accordion");
    for (let i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");

            let panel = this.nextElementSibling;
            if (panel.style.display === "block") {
              panel.style.display = "none";
            } else {
              panel.style.display = "block";
            }
        });
    }
}

function modalPopup() {
    let modal = document.getElementById("results-modal");

    let closeSpan = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    closeSpan.onclick = function() {
        modal.style.display = "none";
        document.getElementById('fail-message').style.display = "none";
        document.getElementById('success-message').style.display = "none";
    };

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
            document.getElementById('fail-message').style.display = "none";
            document.getElementById('success-message').style.display = "none";
        }
    }
}

function switchTextsInModal(hasPassed) {
    if (hasPassed) {
        document.getElementById('fail-message').style.display = "none";
        document.getElementById('success-message').style.display = "block";
    } else {
        document.getElementById('fail-message').style.display = "block";
        document.getElementById('success-message').style.display = "none";
    }
}