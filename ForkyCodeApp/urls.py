"""ForkyCode URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from ForkyCodeApp.Views import home, challenge, session, leaderbords, forum, landing, profile

app_name = 'ForkyCodeApp'
urlpatterns = [
    url(r'^home$', home.Home.as_view(), name="Home"),
    url(r'^login$', session.Login.as_view(), name="Login"),
    url(r'^logout$', session.logoutSession, name="Logout"),
    url(r'^signup$', session.Signup.as_view(), name="Signup"),
    url(r'^challenge$', challenge.Challenge.as_view(), name="Challenge"),
    url(r'^get_leader_data$', leaderbords.getLeaderData, name="GetLeaderData"),
    url(r'^get_forum_data$', forum.getForumData, name="GetForumData"),
    url(r'^createEntry$', forum.CreateEntry.as_view(), name="CreateEntry"),
    url(r'^loadChallenges$', challenge.loadChallenges, name="LoadChallenges"),
    url(r'^get_forum_entry_data$', forum.getForumEntryData, name="GetForumEntryData"),
    url(r'^createComment$', forum.postComment, name="CreateComment"),
    url(r'^$', landing.Landing.as_view(), name="Landing"),
    url(r'^get_profile_data$', profile.loadProfile , name="GetProfileData"),
    url(r'^.*$', home.Default.as_view(), name="Default"),
]
