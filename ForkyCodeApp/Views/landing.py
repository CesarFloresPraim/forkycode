from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.http import JsonResponse, HttpResponse
from ForkyCodeApp.models import Challenge, Category

class Landing(TemplateView):
    template_name = 'Landing/landing.html'

    def get(self, request, *args, **kwargs):
        if 'forky' in request.session:
            return redirect('ForkyCodeApp:Home')

        return render(request, self.template_name)

