from django.http import JsonResponse
from ForkyCodeApp.models import Challenge as ChallengeModel
from ForkyCodeApp.models import Forker

def loadProfile(request):

    email = request.session['forky']['email']
    userRequested = Forker.objects.get(email__exact=email)

    html_info = """
                <div class="profile-section-title">
                    <p>INFO</p>
                </div>
                <div class="profile-circle profile-color center">
                    <h6 class="initials">{0}</h6>
                </div>
                <div class="profile-section-content center">
                     <h6>{1}</h6>
                    <p>{2}</p>
                </div>
                            """.format(userRequested.firstname[:1]+userRequested.lastname[:1],
                                       userRequested.firstname + " " + userRequested.lastname,
                                       userRequested.email)
    html_extra = """
            <div class="row card">
                <div class="col s12">
                    <div class="profile-section-title white">
                        <p>LANGUAGE</p>
                    </div>
                    <div class="profile-section-content">
                        <p>{0}</p>
                    </div>
                </div>
            </div>

            <div class="row card">
                <div class="col s12">
                    <div class="profile-section-title white">
                        <p>POINTS</p>
                    </div>
                    <div class="profile-section-content">
                        <span class="forks-reward">{1}
                            <i class="material-icons">restaurant</i>
                        </span>
                    </div>
                </div>
            </div>
                            """.format(userRequested.favorite_language,
                                       userRequested.points)

    return JsonResponse({
        'html_info': html_info,
        'html_extra': html_extra
    })


