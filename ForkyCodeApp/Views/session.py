from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import hashers
from ForkyCodeApp.models import Forker
from django.contrib.auth import logout

def logoutSession(request):
    if 'forky' in request.session:
        try:
            del request.session['forky']
            logout(request)
        except KeyError:
            pass
    return redirect('ForkyCodeApp:Landing')

class Login(TemplateView):
    template_name = 'Login/index.html'

    def get(self, request, *args, **kwargs):

        if 'forky' in request.session:
            return redirect('ForkyCodeApp:Home')

        return render(request, self.template_name)

    def post(self, request):
        email = request.POST['email']
        password = request.POST['password']

        try:
            userRequested = Forker.objects.get(email__exact=email)
        except ObjectDoesNotExist:
            return render(request, self.template_name, {'error': 'Invalid email or password'})
        args = {
            'email': email,
        }
        if hashers.check_password(password,userRequested.password):
            request.session['forky'] = {
                'name': userRequested.firstname,
                'lastname': userRequested.lastname,
                'email': userRequested.email
            }
            return redirect('ForkyCodeApp:Home')
        else:
            return render(request, self.template_name, {'error': 'Invalid email or password'})

class Signup(TemplateView):
    template_name = 'Signup/index.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request):
        email = request.POST['email']
        password = request.POST['password']
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']

        try:
            Forker.objects.get(email__exact=email)
            return render(request, self.template_name, {'error': '* Email already been used *'})
        except ObjectDoesNotExist:
            newUser = Forker.objects.create(email=email, password=hashers.make_password(password), firstname=firstname, lastname=lastname)
        args = {
            'email': email,
        }

        request.session['forky'] = {
            'name': newUser.firstname,
            'lastname': newUser.lastname,
            'email': newUser.email
        }
        response = redirect('ForkyCodeApp:Home')
        return response