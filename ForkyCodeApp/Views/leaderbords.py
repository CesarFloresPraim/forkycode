from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.http import HttpResponse, JsonResponse
from ForkyCodeApp.models import Forker

def getLeaderData(request):

    top3 = Forker.objects.all().order_by('-points')[:3]
    users = Forker.objects.all().order_by('-points')[3:10]
    html_codigo = ""
    color = ''
    for index, top in enumerate(top3):
        if index == 0:
            color = '#fdd835'
        elif index == 1:
            color = '#bdbdbd'
        elif index == 2:
            color = '#d84315'

        html_codigo += """
                        <li class="collection-item">
                        <div>
                        <span class="fav_lang"><span class="forks-points">{0}<i
                            class="tiny material-icons">restaurant</i> </span>({2})</span> {1}<a href="#!"
                            class="secondary-content mail-icon"><i
                            class="material-icons" style="color:{3}">stars</i></a></div>
                        </li>
                    """.format(top.points, top.firstname + " " + top.lastname, top.favorite_language, color)
    for user in users:
        html_codigo += """
        <li class="collection-item">
            <div>
                <span class="fav_lang"><span class="forks-points">{0}<i
                    class="tiny material-icons">restaurant</i> </span>({2})</span> {1}</div>
        </li>
        """.format(user.points, user.firstname + " " + user.lastname, user.favorite_language)

    args = {
        'html_codigo': html_codigo,
    }
    return JsonResponse(args)
