from django.http import HttpResponse, JsonResponse
from ForkyCodeApp.models import ForumEntry, Forker, ForumComment
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger
from django.views.generic import TemplateView
from django import forms
from django.utils.html import escape

def getForumData(request):

    entries = ForumEntry.objects.all().order_by('-id')
    paginator = Paginator(entries, 10)
    page = request.GET.get('page', 1)
    entries = paginator.page(page)
    html_codigo = ''
    for index, entry in enumerate(entries):
        try:
            author = Forker.objects.get(email__exact=entry.author)
        except ObjectDoesNotExist:
            continue
        except PageNotAnInteger:
            entries = paginator.page(1)
            continue
        html_codigo += """
                <tr>
                    <td class="small-padding">
                        <div>
                            <span class="icon-wrapper">
                                <i class="semi-large material-icons">assignment</i>
                            </span>
                            <a href="#!" id="forum-entry-content" onclick="changeActiveDiv(this.id, this);" 
                                class="topictitle" data-original-title="" title="" data-value={5}>
                                {1}
                            </a>
                            <br>
							by
							<a href="#!" style="color: #AA0000;" class="username-coloured" data-original-title="" title="">
							    {0}
							</a>
							<small>-{2}</small>
                        </div>
                    </td>
                    <td class="stats-col hide-on-small-only small-padding" style="display: table-cell;">
                        <span class="stats-wrapper">
                            {3} <i class="material-icons tiny green-text">arrow_upward</i>
                            <br>
                            {4} <i class="material-icons tiny red-text">arrow_downward</i>
                        </span>
                    </td>
                </tr>
                """.format(author.firstname + " " + author.lastname, escape(entry.title), entry.date, entry.like, entry.dislike, entry.id)

    html_pagination = ''
    if (entries.has_other_pages()):
        html_pagination += '<ul class="pagination center">'
        if (entries.has_previous()):
            html_pagination += '<li onclick="changeActiveDiv(\'forum-content\', this);" data-value="' + str(entries.previous_page_number()) +'"><a href="#!"><i class="material-icons">chevron_left</i></a></li>'
        else:
            html_pagination += '<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>'
        for i in entries.paginator.page_range:
            if(entries.number == i):
                html_pagination += '<li class="activo" onclick="changeActiveDiv(\'forum-content\', this);" data-value="' + str(i) +'"><a href="#!">'+ str(i) +'</a></li>'
            else:
                html_pagination += '<li class="waves-effect" onclick="changeActiveDiv(\'forum-content\', this);" data-value="' + str(i) +'"><a href="#!">'+ str(i) +'</a></li>'
        if (entries.has_next()):
            html_pagination += '<li onclick="changeActiveDiv(\'forum-content\', this);" data-value="' + str(entries.next_page_number()) +'"><a href="#!"><i class="material-icons">chevron_right</i></a></li>'
        else:
            html_pagination += '<li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>'
        html_pagination += '</ul>'

    args = {
        'html_forum': html_codigo,
        'html_pagination': html_pagination
    }
    return JsonResponse(args)


class CreateEntry(TemplateView):
    def post(self, request):
        title = request.POST['title']
        desc = request.POST['desc']
        author_id = request.session['forky']['email']

        f = forms.CharField()

        title = f.clean(title)
        desc = f.clean(desc)

        try:
            author = Forker.objects.get(email__exact=author_id)
        except ObjectDoesNotExist:
            return JsonResponse(status=500,data={})

        ForumEntry.objects.create(title=title, description=desc, author=author,like=0, dislike=0)

        return JsonResponse(status=200,data={})


def getForumEntryData(request):
    id = request.GET.get('id', 1)
    entry = ForumEntry.objects.get(id=id)
    try:
        author = Forker.objects.get(email__exact=entry.author)
    except ObjectDoesNotExist:
        return JsonResponse(status=500, data={})

    html_codigo = """
                    <div id="entry-info" class="panel-head" data-value="{4}">
                        <p href="#!" class="topictitle" data-original-title="" title="">
                            <strong>
                                {1}
                            </strong>
                        </p>
                        by
                        <a href="#!" style="color: #AA0000;" class="username-coloured" data-original-title="" title="">
                            {0}
                        </a>
                        <small>-{2}</small><br><hr>
                        <p>{3}</p>
                    </div>
                    """.format(author.firstname + " " + author.lastname, escape(entry.title), entry.date, escape(entry.description), entry.id)


    html_comments = ''
    comment = ForumComment.objects.filter(entry=entry.id).order_by('id')
    for index, comment in enumerate(comment):
        try:
            comment_author = Forker.objects.get(email__exact=comment.author)
        except ObjectDoesNotExist:
            continue
        html_comments += """
                    <div class="small-padding-comment">
                        <p id="comment" style="margin-bottom: 1%;">
                            <a href="#!" style="color: #AA0000;" class="username-coloured" data-original-title="" title="">
                                {0}:
                            </a>
                            {1}
                        </p>
                        <small>{2}</small>
                    </div>
                    """.format(comment_author.firstname + " " + comment_author.lastname, escape(comment.description), entry.date, entry.like)
    html_comments += '<div class="small-padding-comment"><p>*No comments*</p></div>' if html_comments == '' else '<br>'
    args = {
        'html_entry': html_codigo,
        'html_comments': html_comments,
    }
    return JsonResponse(args)

def postComment(request):
    desc = request.POST['reply']
    entry_id = request.POST['entry_id']
    author_id = request.session['forky']['email']

    f = forms.CharField()

    desc = f.clean(desc)

    try:
        author = Forker.objects.get(email__exact=author_id)
        entry = ForumEntry.objects.get(id=entry_id)
    except ObjectDoesNotExist:
        return JsonResponse(status=500,data={'au':author_id,'asd':entry_id})

    ForumComment.objects.create(description=desc,  author=author,entry=entry,like=0, dislike=0)

    return JsonResponse(status=200,data={})