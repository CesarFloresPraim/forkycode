from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
import requests
import ForkyCodeApp.Business.util.Builder as Builder
import logging
from ForkyCodeApp.models import Challenge as ChallengeModel
from ForkyCodeApp.models import Category
import ForkyCodeApp.Business.Adapters.ChallengeAdapter as Adapter
import ForkyCodeApp.Business.util.Score as sc


class Challenge(TemplateView):
    template_name = 'Editor/editor.html'

    def get(self, request, *args, **kwargs):
        if 'forky' in request.session:
            qIDstr = request.GET.get('id')

            if not qIDstr:
                return redirect('ForkyCodeApp:Home')

            qID = int(qIDstr)

            try:
                challenge_adapter = Adapter.Challenge(qID)
            except ObjectDoesNotExist:
                # Render 404 page
                return redirect('ForkyCodeApp:Home')

            data = {
                "user": request.session['forky'],
                'challenge_id': str(qID),
                'description': challenge_adapter.challenge.description,
                'func_name': challenge_adapter.challenge.function_name,
                'test_cases': challenge_adapter.render_test_cases_template(),
                'func_def': challenge_adapter.generate_function()
            }
            return render(request, self.template_name, data)
        else:
            return redirect('ForkyCodeApp:Login')

    def post(self, request):
        api_url = 'http://forky_api:4321/code'

        chID = request.POST.get("challenge_id")
        src_code = request.POST.get("input_code")
        is_real = True if request.POST.get("is_real") == 'true' else False

        try:
            challenge_adapter = Adapter.Challenge(chID)
        except ObjectDoesNotExist:
            return JsonResponse({'error': 'Challenge not found'})

        try:
            jb = Builder.JsonBuilder(language="python",
                                     func_name=challenge_adapter.challenge.function_name,
                                     return_type=challenge_adapter.test_cases[0].correct_type.type,
                                     test_params=challenge_adapter.get_params_formated(is_real),
                                     correct_results=challenge_adapter.get_correct_results_formated(is_real),
                                     source_code=src_code)
        except Exception as e:
            logging.basicConfig(filename='challenge.log', level=logging.DEBUG)
            logging.error('Builder exception: ' + str(e))
            return JsonResponse({'error': 'Error. Try again later'})

        json_data, err = jb.build_json()
        if err:
            # Expected error
            return JsonResponse({'error': str(json_data)})

        res = None
        try:
            res = requests.post(url=api_url, data=json_data, headers={"Content-Type": "application/json"})
        except Exception as e:
            logging.basicConfig(filename='challenge.log', level=logging.DEBUG)
            logging.error('post exception: ' + str(e))
            return JsonResponse({'error': 'Error. Try again later'})

        if res.json().get('console_error', False):
            return JsonResponse({'error': res.json()['console_error']})

        score_pretty, has_passed = sc.Score(chID, res.json()['test_results'], is_real, get_user_id(request)).render_score()

        return JsonResponse(
            {
                'test_results': res.json()['test_results'],
                'is_real': is_real,
                'score': {'text': score_pretty, 'has_passed': has_passed}
            })


def get_user_id(request):
    if 'forky' in request.session:
        return request.session['forky']['email']
    return ""


def loadChallenges(request):
    html_code = ""
    categories = Category.objects.all()

    for category in  categories:
        html_code += """
                        <li>
                            <div class="collapsible-header"
                                style="text-transform: uppercase">
                                <i class="material-icons">code</i>
                                {0}
                            </div>
                        <div class="collapsible-body" 
                        style="padding: 1rem!important;
                         background-color: rgb(232, 233, 236)">
                        """.format(category.name)
        challenges = ChallengeModel.objects.filter(category__name=category.name)
        i = 1;
        for challenge in challenges:
            html_code += """
                        <div class="flex space-task task">
                            <h6 class="task-circle complete center">
                                {2}
                            </h6>
                            <a href="/challenge?id={1}" class="challenge_name" style="margin: 6px">{0}</a>
                        </div>
                """.format(challenge.function_name, challenge.id, i)
            i += 1

    html_code += '</div></li>' if html_code != '' else ''
    return JsonResponse({'html_code': html_code})