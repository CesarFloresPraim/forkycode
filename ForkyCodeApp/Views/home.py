from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.http import JsonResponse, HttpResponse
from ForkyCodeApp.models import Challenge, Category
from django.template import Context, Template


class Home(TemplateView):
    template_name = 'Home/index.html'

    def get(self, request, *args, **kwargs):
        if 'forky' in request.session:
            categories = Category.objects.all()
            challenges_html = """<div class="row">"""
            for cat in categories:
                challengeCat = Challenge.objects.filter(category__name=cat.name).last()

                challenges_html += """
                        <div class="col s12 m6">
                            <div class="card">
                                <div class="card-image">
                                    <img src="../../static/media/challenges2.jpg" class="limit-card-heigth">
                                    <span class="card-title">{3} <span class="name-of-algorithm"><i
                                            class="material-icons">arrow_forward</i>
                                            {0}()</span></span>
                                    <a class="btn-floating halfway-fab waves-effect waves-light red"><i
                                            class="material-icons">check</i></a>
                                </div>
                                <div class="card-content">
                                    <p>{1} <span class="forks-reward">+{2}<i
                                            class="material-icons">restaurant</i></span></p>
                                </div>
                            </div>
                        </div>
                    """.format(challengeCat.function_name, challengeCat.description, challengeCat.points, cat.name)

            challenges_html += "</div>"

            return render(request, self.template_name,{"user":request.session['forky'], "challenges": challenges_html})
        else:
            return redirect('ForkyCodeApp:Login')


class Default(TemplateView):

    def get(self, request, *args, **kwargs):
        return redirect('ForkyCodeApp:Home')
