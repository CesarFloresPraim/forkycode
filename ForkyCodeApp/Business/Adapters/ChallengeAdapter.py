from ForkyCodeApp.models import Challenge as ChallengeModel, TestCase, Parameter, ParameterConfig


class Challenge:
    def __init__(self, qID):
        self.challenge = ChallengeModel.objects.get(id=qID)
        self.test_cases = TestCase.objects.filter(challenge=self.challenge)
        self.param_config = ParameterConfig.objects.filter(challenge=self.challenge)

    def render_test_cases_template(self):
        test_dict = {}
        index = 1
        for test in self.test_cases:
            if not test.is_real:
                parameters = Parameter.objects.filter(test_case=test)
                params_list = []
                for param in parameters:
                    params_list.append(param.parameter_value)
                test_dict[index] = {'input': params_list, 'expected': test.correct_result}
                index += 1
        return test_dict

    def get_param_names(self):
        params = ""
        sorter = 0
        counter = 1
        max_loops = self.param_config.count() + 1
        while sorter < self.param_config.count():
            for config in self.param_config:
                if sorter == config.order:
                    params += config.parameter_name + ", "
                    sorter += 1
            if counter == max_loops:
                return ""
            counter += 1
        return params.rstrip(', ')

    def generate_function(self):
        body = "def {}({}):\n  "
        return body.format(self.challenge.function_name, self.get_param_names())

    def type_adapter(self, data_type, value):
        if data_type == 'integer':
            return {'number': str(value)}
        elif data_type == 'float':
            return {'number': str(value)}
        elif data_type == 'array':
            return {'list': str(value)}
        elif data_type == 'dict':
            return {'dict': str(value)}
        else:
            return {data_type: value}

    def get_params_formated(self, is_real=False):
        array = []
        for test in self.test_cases:
            batch = []
            if test.is_real == is_real:
                parameters = Parameter.objects.filter(test_case=test)
                for param in parameters:
                    batch.append(self.type_adapter(param.parameter_config.parameter_type.type, param.parameter_value))
                array.append(batch)
        return array

    def get_correct_results_formated(self, is_real=False):
        array = []
        for test in self.test_cases:
            if test.is_real == is_real:
                array.append(str(test.correct_result))
        return array
