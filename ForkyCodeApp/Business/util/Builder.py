import json
import uuid


# Compile code to be sent in a string
class CodeGenerator:

    def __init__(self, language, func_name, return_type, test_params, correct_results, source_code):

        self.hash = uuid.uuid4()
        self.language = language
        self.func_name = func_name
        self.return_type = return_type
        self.test_params = test_params
        self.correct_results = correct_results
        self.source_code = source_code

    def build_exec_file(self):
        if self.language == "python":
            return self.build_exec_file_py()

    def build_exec_file_py(self):
        dependencies = "import json\n"
        main_func = self.build_main_py()
        return dependencies + self.source_code + "\n" + main_func

    def build_main_py(self):
        test_cases = self.get_number_of_tests()

        main = "def main():\n\tresults = []"

        for i in range(0, test_cases):
            main += "\n\tresults.append({}({}))".format(self.func_name, self.get_test_case_params(i))
        main += self.get_dumper_py()
        main += "\nmain()"
        return main

    def get_dumper_py(self):
        result_filename = str(self.hash) + "/data.json"
        code = '\n\ttemp_dict = {"test_results": results}'
        code += "\n\twith open('{}', 'w') as outfile:".format(result_filename)
        code += "\n\t\tjson.dump(temp_dict, outfile)"
        code += "\n"
        return code

    def get_number_of_tests(self):
        return len(self.correct_results)

    def get_test_case(self, case):
        return self.test_params[case]

    def get_test_case_params(self, case):
        test_case = self.get_test_case(case)
        param_string = ''

        for param in test_case:
            if "number" in param:
                param_string += param["number"]
            elif "string" in param:
                param_string += '"' + param["string"] + '"'
            elif "list" in param:
                param_string += param["list"]
            elif "dict" in param:
                param_string += param["dict"]
            param_string += ", "
        return param_string.rstrip(', ')


# Build json to be sent
class JsonBuilder(CodeGenerator):

    def __init__(self, language, func_name, return_type, test_params, correct_results, source_code):

        super().__init__(language, func_name, return_type, test_params, correct_results, source_code)

    def build_json(self):
        error_message = self.validate_types()
        if error_message:
            return error_message, True

        json_dict = {
            "hash": str(self.hash),
            "language": str(self.language),
            "func_name": str(self.func_name),
            "return_type": str(self.return_type),
            "total_params": str(len(self.test_params)),
            "test_params": self.test_params,
            "correct_results": self.correct_results,
            "test_results": [],
            "source_code": self.build_exec_file()
        }

        return json.dumps(json_dict), False

    def is_source_code_valid(self):
        py_invalid_keywords = ['import']
        result = True
        if self.language == "python":
            if any(kword in self.source_code for kword in py_invalid_keywords):
                result = False
        return result

    def validate_types(self):
        message = {}
        if not isinstance(self.test_params, list):
            message["test_params"] = "ERROR: provided test_params is not a list\n"
        if not isinstance(self.correct_results, list):
            message["correct_results"] = "ERROR: provided correct_results is not a list\n"
        if len(self.test_params) != len(self.correct_results):
            message["miss_match"] = "ERROR: provided correct_results doesn't match with the provided test_params\n"
        if len(self.source_code) == 0:
            message["source_code_1"] = "ERROR: source_code not provided\n"
        if not self.is_source_code_valid():
            message["source_code_2"] = "ERROR: keyword not supported\n"
        return message
