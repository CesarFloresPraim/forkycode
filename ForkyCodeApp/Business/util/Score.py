from ForkyCodeApp.models import Challenge as ChallengeModel, TestCase, Forker, CompletedChallenges
import logging


class Score:
    def __init__(self, challenge_id, user_results, is_real, forker_id):
        self.challenge = ChallengeModel.objects.get(id=challenge_id)
        self.test_cases = TestCase.objects.filter(challenge=self.challenge).filter(is_real=is_real)
        self.forker = self.getForkerObject(forker_id)
        self.completedChallenges = None
        self.user_results = user_results
        self.is_real = is_real
        self.score_dict = self.get_score_dict()

    def get_score_dict(self):
        logging.basicConfig(filename='challenge.log', level=logging.DEBUG)
        if not self.lengths_equal():
            logging.error("ForkyCodeApp.Business.util.Score: results length mismatch")
            return {}
        total = 0

        for i, test in enumerate(self.test_cases):
            if test.is_real == self.is_real:
                if test.correct_result == str(self.user_results[i]):
                    total += 1
        return {'up': total, 'down': len(self.user_results)}

    def lengths_equal(self):
        return True if self.test_cases.count() == len(self.user_results) else False

    def has_passed(self):
        return True if self.score_dict['up'] / self.score_dict['down'] == 1 else False

    def render_score(self):
        self.setPoints()
        if not self.score_dict:
            return "0/0", False
        return str(self.score_dict['up']) + '/' + str(self.score_dict['down']), self.has_passed()

    def render_calculated_score(self):
        self.setPoints()
        if not self.score_dict:
            return "0", False
        return str(self.score_dict['up'] / self.score_dict['down']), self.has_passed()

    def setChallengeCompletedByForker(self):
        if not self.hasCompletedChallenge():
            completedChallenges = CompletedChallenges.objects.create(challenge=self.challenge, forker=self.forker)
            completedChallenges.save()

    def setPoints(self):
        if not self.hasCompletedChallenge():
            if self.is_real and self.has_passed():
                self.forker.points += self.challenge.points
                self.forker.save()
                self.setChallengeCompletedByForker()

    def hasCompletedChallenge(self):
        try:
            self.completedChallenges = CompletedChallenges.objects.get(challenge=self.challenge, forker=self.forker)
        except CompletedChallenges.DoesNotExist:
            return False
        return True

    def getForkerObject(self, forker_id):
        forker = None
        try:
            forker = Forker.objects.get(email=forker_id)
        except Forker.DoesNotExist:
            pass
        return forker
