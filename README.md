# ForkyCode

## Automated Setup

### Prerequisites
1. Install Docker [how to install](https://docs.docker.com/install/)
1. Install Docker Compose [how to install](https://docs.docker.com/compose/install/)

### Instructions
1. Clone repository using `git clone https://bitbucket.org/<your_username>/CesarFloresPraim/forkycode.git`
1. Change into the ForkyCode directory `cd forkycode`
1. Run the following command and follow the on-screen prompts.

        sh install.sh

1. **Setup Really**

## Production Environment

### Routes
Url: [Forkycode.com](http://forkycode.com)

Admin Url: [Forkycode.com/admin](http://forkycode.com/admin)

### Deploy
#### Prerequisites
1. Create an SSH key [how to create](https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/create-with-openssh/)
1. Add your SSH key to the Droplet. (*Owner must do this)

#### Instructions
1. Connect to the server by ssh `ssh root@forkycode.com`
1. Change into the ForkyCode directory `cd forkycode`
1. Run the following command.

        sh deploy.sh

1. **Deploy Really**


## Admin Manage Tutorials

* Go to the admin url and login *

###How to create a Challenge?

#### 1. Create a **Category**
* Enter a name for the category

**NOTE:** skip this step if you already have the category you want.

#### 2. Create a **Challenge**
###### Provide de following:
* _description_
* _level_ of difficulty
* _function name_
* _points_ as a reward
* _category_

#### 3. Create a **Test Case**
##### NOTE: Each challenge can have multiple test cases
###### Provide de following:
* _challenge_
* _is_real_: false if you want this test case to be shown to the user
* _correct_result_: the correct result for this test case. This is a string.
* _correct_type_: the data type for the result.

#### 4. Create a **Parameter**
##### NOTE: Each parameter needs a blueprint so it can be replicated multiple times
##### NOTE 2: You will need to create a param configuration for any param you want in the function

##### 4.1 Create a **Parameter Configuration**
###### Provide de following:
* _challenge_
* parameter name: name to be displayed in the function
* parameter type
* order: position for the parameter to be displayed (starts in 0)

##### 4.2 Create a **Parameter based on the Configuration**
###### Provide de following:
* test_case
* parameter_configuration
* value: parameter's value