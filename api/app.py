from flask import Flask, request, jsonify
import subprocess
import json
import os

app = Flask(__name__)


@app.route('/')
def hello_world():
    return '<span>.__( .)< (CHANWICH)</span><br><span>\___)</span><br>'


@app.route('/code', methods=['POST'])
def handle_code_exec():
    jonson_dict = request.get_json()
    is_valid, message = validate_json_data(jonson_dict)

    if not is_valid:
        return json.dumps(message)

    try:
        # Default files
        os.makedirs(jonson_dict["hash"], exist_ok=True)
        filename = jonson_dict["hash"] + "/code.py"
        result_data_file = jonson_dict["hash"] + "/data.json"

        # Create program file
        file = open(filename, "w")
        file.write(jonson_dict["source_code"])
        file.close()

        # Execute code file
        process = exec_code(jonson_dict["language"], filename)

        # When execution has finished
        # Append results to json form generated json file
        if process.returncode == 0:
            with open(result_data_file, 'r') as result_file:
                result_dict = json.load(result_file)
            jonson_dict["test_results"] = result_dict["test_results"]
        else:
            jonson_dict["console_error"] = filer_error_py(str(process.stderr))
        # Cleanup
        subprocess.run(['rm', '-rf', jonson_dict["hash"]])

        response = app.response_class(
            response=json.dumps(jonson_dict),
            status=200,
            mimetype='application/json'
        )

        return response

    except Exception as e:
        subprocess.run(['rm', '-rf', jonson_dict["hash"]])
        response = app.response_class(
            response=json.dumps({"console_error": "Try again later :)"}),
            status=400,
            mimetype='application/json'
        )
        return response


def exec_code(language, filename):
    if language == "python":
        return exec_python(filename)
    elif language == "node":
        return exec_nodejs(filename)


def exec_nodejs(filename):
    return subprocess.run(['node', filename], timeout=10, stderr=subprocess.PIPE)


def exec_python(filename):
    return subprocess.run(['python3', filename], timeout=10, stderr=subprocess.PIPE)


def validate_json_data(jonson):
    is_valid = True
    message = {}
    if jonson["hash"] == '/' or jonson["hash"] == "" or jonson["hash"] == "api":
        is_valid = False
        message["HASH"] = "HASH value is invalid"
    if jonson["source_code"] == "":
        is_valid = False
        message["SOURCE_CODE"] = "SOURCE_CODE value is empty"
    return is_valid, message


errorTypes = ['AssertionError', 'AttributeError', 'EOFError', 'FloatingPointError', 'GeneratorExit', 'ImportError',
              'IndexError', 'KeyError', 'KeyboardInterrupt', 'MemoryError', 'NameError', 'NotImplementedError',
              'OSError', 'OverflowError', 'ReferenceError', 'RuntimeError', 'StopIteration', 'SyntaxError',
              'IndentationError', 'TabError', 'SystemError', 'SystemExit', 'TypeError', 'UnboundLocalError',
              'UnicodeError', 'UnicodeEncodeError', 'UnicodeDecodeError', 'UnicodeTranslateError',
              'ValueError', 'ZeroDivisionError']


def filer_error_py(err_str):
    start = 0
    for error in errorTypes:
        if error in err_str:
            start = err_str.rfind(error)
    return err_str[start:].rstrip("\\n'")


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4321, debug=True)
