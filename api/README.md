 ##JSON structure
 
     {
        "hash": <hash_string>,
        "language": <string>,
        "func_name": <string>,
        "return_type": <string>,
        "total_params": <string>,
        "test_params": [
            [
                {<type_string>: <string>},
                {<type_string>: <string>}
            ],
            [
                {<type_string>: <string>},
                {<type_string>: <string>}
            ]
        ],
        "correct_results": [
            <string>,
            <string>
        ],
        "test_results" : [],
        "source_code": <string>
    }

###Example:

    {
        "hash": "xxx",
        "language": "python",
        "func_name": "foo",
        "return_type": "number",
        "total_params": "2",
        "test_params": [
            [
                {"number": "1"},
                {"list": "[1,2,3]"}
            ],
            [
                {"number": "3.5"},
                {"list": "['1','2','3']"}
            ]
        ],
        "correct_results": [
            "5",
            "6"
        ],
        "test_results" : [],
        "source_code": "\nimport json\n\ndef foo(num, dos):\n\treturn num * dos\n\n# Generated\ndef main():\n\tresults = []\n\tresults.append(foo(1, 2))\n\tresults.append(foo(3, 4))\n\tdict = {\"test_results\":results}\n\twith open('xxx/data.json', 'w') as outfile:\n\t\tjson.dump(dict, outfile)\nmain()\n"
    }
    